package storages

import (
	"gitlab.com/antoxa2614/go-rpc-user/internal/db/adapter"
	"gitlab.com/antoxa2614/go-rpc-user/internal/infrastructure/cache"
	ustorage "gitlab.com/antoxa2614/go-rpc-user/internal/modules/user/storage"
)

type Storages struct {
	User ustorage.Userer
}

func NewStorages(sqlAdapter *adapter.SQLAdapter, cache cache.Cache) *Storages {
	return &Storages{
		User: ustorage.NewUserStorage(sqlAdapter, cache),
	}
}
