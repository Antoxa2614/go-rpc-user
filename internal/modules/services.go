package modules

import (
	"gitlab.com/antoxa2614/go-rpc-user/internal/infrastructure/component"
	uservice "gitlab.com/antoxa2614/go-rpc-user/internal/modules/user/service"
	"gitlab.com/antoxa2614/go-rpc-user/internal/storages"
)

type Services struct {
	User          uservice.Userer
	UserClientRPC uservice.Userer
}

func NewServices(storages *storages.Storages, components *component.Components) *Services {
	return &Services{
		User: uservice.NewUserService(storages.User, components.Logger),
	}
}
