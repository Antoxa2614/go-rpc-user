package modules

import (
	"gitlab.com/antoxa2614/go-rpc-user/internal/infrastructure/component"
	ucontroller "gitlab.com/antoxa2614/go-rpc-user/internal/modules/user/controller"
)

type Controllers struct {
	User ucontroller.Userer
}

func NewControllers(services *Services, components *component.Components) *Controllers {
	userController := ucontroller.NewUser(services.User, components)

	return &Controllers{
		User: userController,
	}
}
