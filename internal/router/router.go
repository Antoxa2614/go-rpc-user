package router

import (
	"net/http"

	"gitlab.com/antoxa2614/go-rpc-user/internal/infrastructure/component"
	"gitlab.com/antoxa2614/go-rpc-user/internal/infrastructure/middleware"
	"gitlab.com/antoxa2614/go-rpc-user/internal/modules"
)

func NewApiRouter(controllers *modules.Controllers, components *component.Components) http.Handler {
	r := chi.NewRouter()

	r.Route("/api", func(r chi.Router) {
		r.Route("/1", func(r chi.Router) {
			authCheck := middleware.NewTokenManager(components.Responder, components.TokenManager)
			r.Route("/user", func(r chi.Router) {
				userController := controllers.User
				r.Route("/profile", func(r chi.Router) {
					r.Use(authCheck.CheckStrict)
					r.Get("/", userController.Profile)
				})
			})
		})
	})

	return r
}
